jQuery(document).ready(function () {
    jQuery(".left-menu-trigger button").on("click", function () {
        jQuery("body").toggleClass("side-menu-activated");
        //jQuery(".leftside-menu").toggleClass("side-menu-activated");
    });
    jQuery(".leftside-menu-wrapper>ul>li>a").on("click", function (e) {
        jQuery(this).parent().siblings().removeClass("activated");
        jQuery(this).parent().addClass("activated");
        console.log("Log");
    });

    jQuery("#patient-reg-type").on('change', function () {
        const selectedItem = $("#patient-reg-type option:selected").val();
        if (selectedItem === 'general') {
            jQuery("#patient-reg-doc").hide();
        } else {
            jQuery("#patient-reg-doc").show();
        }

    })
    jQuery("#toggleSwitch").on('change', function (event) {
        if (event.target.checked) {
            jQuery("#toggleText").text('MIX')
        } else {
            jQuery("#toggleText").text('ADD')
        }
    })
})
